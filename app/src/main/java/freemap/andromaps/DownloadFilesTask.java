package freemap.andromaps;



import android.content.Context;


import java.io.InputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;



public abstract class DownloadFilesTask extends HTTPCommunicationTask {

	String[] localFiles;
	
	public DownloadFilesTask(Context ctx,  String[] urls, String[] localFiles, String alertMsg, Callback callback, int taskId)
	{
		super(ctx,urls,alertMsg,callback,taskId);
		this.localFiles=localFiles;
	}
		
	public String doInBackground(Void... unused)
	{
		
		HttpURLConnection conn = null;
		
		try {
			String msg="Failed to download files:";
			int errors=0;
			for(int i=0; i<urls.length; i++) {
				URL url = new URL(urls[i]);
				conn = (HttpURLConnection)url.openConnection();
				InputStream in = conn.getInputStream();
				if(in!=null) {
					doWriteFile(in, localFiles[i]);
				}else {
					errors++;
					msg += " " + (i+1);
				}
				conn.disconnect();
			} 
			setSuccess(errors!=urls.length);
			return errors==0 ? "Successfully downloaded" :
				(errors == urls.length ? "Failed to download files": msg);
		} catch(Exception e) {
			if(conn!=null) {
				conn.disconnect();
			}
			return e.getMessage();
		}
	}
	
	public abstract void doWriteFile(InputStream in, String localFile) throws IOException;
}
