package freemap.andromaps;

import android.content.Context;



import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;



public class HTTPUploadTask extends HTTPCommunicationTask {
	
	String postData;
	String username,password;
	
	public HTTPUploadTask(Context ctx, String url, String postData, String alertMsg, Callback callback, int taskId) {
		super(ctx,new String[]{url},alertMsg,callback,taskId);	
		this.postData=postData;
	}
	
	
	public String doInBackground(Void... unused) {
		HttpURLConnection conn = null;
		try {
			URL url = new URL(urls[0]);
			conn = (HttpURLConnection)url.openConnection();
			if(username!=null && password!=null) {
				String details=username+":"+password;
				conn.setRequestProperty("Authorization", "Basic " +
						Base64.encodeBytes(details.getBytes()));
			}
			conn.setDoOutput(true);
			conn.setFixedLengthStreamingMode(postData.length());
			OutputStream out = conn.getOutputStream();
			out.write(postData.getBytes());
			boolean success = conn.getResponseCode()==200;
			String response = "";
			if(success) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String line;
				while ((line = reader.readLine()) != null) {
					response += line;
				}
			}
			setSuccess(success);
			setAdditionalData(response);
			return (success) ? "Successfully uploaded" : "Upload failed with HTTP code " +
						conn.getResponseCode();
		}catch(Exception e){
			return e.getMessage();
		} finally {
			if(conn!=null) {
				conn.disconnect();
			}
		}
	}
	
	public void setLoginDetails(String username, String password) {
		this.username=username;
		this.password=password;
	}
	
	public void setPostData(String postData)
	{
	    this.postData = postData;
	}
}
